/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject * TutorialApplication::CreateCube(float size)
{
	ManualObject *triangle = mSceneMgr->createManualObject();
	// BaseWhiteNoLighting - a material that does not use light and all verts are color white
	// OT_TRIANGLE_LIST - 3 vertices per triangle
	triangle->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);

	// Setup vertex position

	// Front
	triangle->position(size, size, size); // UR
	triangle->position(size, -size, size); // LR
	triangle->position(-size, -size, size); // LL
	triangle->position(-size, size, size); // UL

	// Back
	triangle->position(size, size, -size); // UR
	triangle->position(size, -size, -size); // LR
	triangle->position(-size, -size, -size); // LL
	triangle->position(-size, size, -size); // UL



	triangle->index(0);
	triangle->index(2);
	triangle->index(1);

	triangle->index(2);
	triangle->index(0);
	triangle->index(3);

	triangle->index(4);
	triangle->index(5);
	triangle->index(6);

	triangle->index(6);
	triangle->index(7);
	triangle->index(4);

	triangle->index(0);
	triangle->index(1);
	triangle->index(4);

	triangle->index(1);
	triangle->index(5);
	triangle->index(4);

	triangle->index(3);
	triangle->index(6);
	triangle->index(2);

	triangle->index(3);
	triangle->index(7);
	triangle->index(6);

	triangle->index(4);
	triangle->index(7);
	triangle->index(3);

	triangle->index(3);
	triangle->index(0);
	triangle->index(4);

	triangle->index(2);
	triangle->index(6);
	triangle->index(1);

	triangle->index(1);
	triangle->index(6);
	triangle->index(5);

	// End drawing
	triangle->end();

	return triangle;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	 // Create your scene here :)
	ManualObject *triangle = mSceneMgr->createManualObject();
	// BaseWhiteNoLighting - a material that does not use light and all verts are color white
	// OT_TRIANGLE_LIST - 3 vertices per triangle
	triangle->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);

	// Setup vertex position

	// Front
	triangle->position(10, 10, 10); // UR
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(1, 0, 1));

	triangle->position(10, -10, 10); // LR
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(1, 0, 1));

	triangle->position(-10, -10, 10); // LL
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(-1, 0, 1));

	triangle->position(-10, 10, 10); // UL
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(-1, 0, 1));


	// Back
	triangle->position(10, 10, -10); // UR
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(1, 0, -1));

	triangle->position(10, -10, -10); // LR
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(1, 0, -1));

	triangle->position(-10, -10, -10); // LL
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(-1, 0, -1));

	triangle->position(-10, 10, -10); // UL
	triangle->colour(ColourValue::Black);
	triangle->normal(Vector3(-1, 0, -1));


	mSceneMgr->setAmbientLight(ColourValue::Black);

	Light* light = mSceneMgr->createLight();
	light->setType(Light::LightTypes::LT_POINT);
	light->setPosition(Vector3(0, 0, 0));
	light->setDiffuseColour(ColourValue(0.5f, 0.5f, 0.0f));
	light->setAttenuation(325, 0.0f, 0.014, 0.0007);


	triangle->index(0);
	triangle->index(2);
	triangle->index(1);

	triangle->index(2);
	triangle->index(0);
	triangle->index(3);

	triangle->index(4);
	triangle->index(5);
	triangle->index(6);

	triangle->index(6);
	triangle->index(7);
	triangle->index(4);

	triangle->index(0);
	triangle->index(1);
	triangle->index(4);

	triangle->index(1);
	triangle->index(5);
	triangle->index(4);

	triangle->index(3);
	triangle->index(6);
	triangle->index(2);

	triangle->index(3);
	triangle->index(7);
	triangle->index(6);

	triangle->index(4);
	triangle->index(7);
	triangle->index(3);

	triangle->index(3);
	triangle->index(0);
	triangle->index(4);

	triangle->index(2);
	triangle->index(6);
	triangle->index(1);

	triangle->index(1);
	triangle->index(6);
	triangle->index(5);


	// End drawing
	triangle->end();

	// Add the object to the scene
	Node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	Node->attachObject(triangle);
	Node->translate(-20, 0, 0);
	// press "r" to check warframe
      
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	int accelaration = 0;
	accelaration = mAccelaration;
	if (mKeyboard->isKeyDown(OIS::KC_L))
	{
		Node->translate((1 + mAccelaration) * evt.timeSinceLastFrame, 0, 0);
		mAccelaration++;
	}
	if (mKeyboard->isKeyDown(OIS::KC_J))
	{
		Node->translate((-1 - mAccelaration)* evt.timeSinceLastFrame, 0, 0);
		mAccelaration++;
	}

	if (mKeyboard->isKeyDown(OIS::KC_I))
	{
		Node->translate(0, (1 + mAccelaration)* evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}


	if (mKeyboard->isKeyDown(OIS::KC_K))
	{
		Node->translate(0, (-1 - mAccelaration)* evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		Node->translate((-1 - mAccelaration) * evt.timeSinceLastFrame, (-1 - mAccelaration) * evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_K)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		Node->translate((1 + mAccelaration)* evt.timeSinceLastFrame, (-1 - mAccelaration)* evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_J)))
	{
		Node->translate((-1 - mAccelaration)* evt.timeSinceLastFrame, (1 + mAccelaration)* evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}

	if ((mKeyboard->isKeyDown(OIS::KC_I)) && (mKeyboard->isKeyDown(OIS::KC_L)))
	{
		Node->translate((1 + mAccelaration)* evt.timeSinceLastFrame, (1 + mAccelaration)* evt.timeSinceLastFrame, 0);
		mAccelaration++;
	}

		Node->yaw(Radian(1) * evt.timeSinceLastFrame);
	

	return true;
}


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
