#pragma once
#include <vector>
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>

using namespace Ogre;
using namespace std;

class Planets 
{
public:
	Planets(SceneNode *node);
	~Planets();
	static Planets *createPlanets(SceneManager *sceneManager, float size, ColourValue colour);

	void setRotate(double speed);

	void setRevolve(double speed);

	void updatePosition(const FrameEvent & evt);

	void setHead(Planets *HeavenlyBody);

	Planets &getHead();

	SceneNode &getNode();

private:
	SceneNode *Node;
	Planets *HeavenlyBodies;
	Radian rotationSpeed;
	Radian revolutionSpeed;
};

