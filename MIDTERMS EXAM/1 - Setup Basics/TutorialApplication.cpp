/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planets.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject * TutorialApplication::DisplaySolarSystem(void)
{
	Planets * Sun = Planets::createPlanets(mSceneMgr, 20,ColourValue(1, 1, 0));
	Sun->setHead(Sun);
	Sun->getNode().setPosition(0, 0, 0);
	Sun->setRotate(7);
	Planet.push_back(Sun);
	

	Planets * Mercury = Planets::createPlanets(mSceneMgr, 3, ColourValue(1, 0, 0));
	Mercury->setHead(Sun);
	Mercury->setRotate(7);
	Mercury->setRevolve(1 * 4.1477);
	Mercury->getNode().setPosition(57.9, 0, 0);
	Planet.push_back(Mercury);

	Planets * Venus = Planets::createPlanets(mSceneMgr, 5, ColourValue(1, 0, 1));
	Venus->setHead(Sun);
	Venus->setRotate(7);
	Venus->setRevolve(1 * 1.6243);
	Venus->getNode().setPosition(108.2, 0, 0);
	Planet.push_back(Venus);

	Planets * Earth = Planets::createPlanets(mSceneMgr, 10, ColourValue(0, 0, 1));
	Earth->setHead(Sun);
	Earth->setRotate(7);
	Earth->setRevolve(1);
	Earth->getNode().setPosition(149.6, 0, 0);
	Planet.push_back(Earth);

	Planets * Luna = Planets::createPlanets(mSceneMgr, 1, ColourValue(1, 1, 1));
	Luna->setHead(Earth);
	Luna->setRotate(7);
	Luna->setRevolve(7);
	Luna->getNode().setPosition(Luna->getHead().getNode().getPosition().x + 30, 0, 0);
	Planet.push_back(Luna);

	Planets * Mars = Planets::createPlanets(mSceneMgr, 8, ColourValue(0, 1, 0));
	Mars->setHead(Sun);
	Mars->setRotate(7);
	Mars->setRevolve(1 * 0.5313);
	Mars->getNode().setPosition(227.9, 0, 0);
	Planet.push_back(Mars);


	return 0;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	DisplaySolarSystem();
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameStarted(const FrameEvent &evt)
{
	for (int i = 0; i < Planet.size(); i++)
	{
		Planet.at(i)->updatePosition(evt);
	}
	return true;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
