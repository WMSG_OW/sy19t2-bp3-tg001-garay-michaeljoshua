#include "Planets.h"
#include "TutorialApplication.h"
#include <vector>
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>


Planets::Planets(SceneNode * node)
{
	Node = node;
}

Planets::~Planets()
{
}

Planets * Planets::createPlanets(SceneManager *sceneManager, float size, ColourValue colour)
{

	ManualObject *triangle = sceneManager->createManualObject();
	// BaseWhiteNoLighting - a material that does not use light and all verts are color white
	// OT_TRIANGLE_LIST - 3 vertices per triangle
	triangle->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	// Setup vertex position

	// Front
	triangle->position(size, size, size); // UR
	triangle->colour(colour);
	triangle->position(size, -size, size); // LR
	triangle->colour(colour);
	triangle->position(-size, -size, size); // LL
	triangle->colour(colour);
	triangle->position(-size, size, size); // UL
	triangle->colour(colour);
	// Back
	triangle->position(size, size, -size); // UR
	triangle->colour(colour);
	triangle->position(size, -size, -size); // LR
	triangle->colour(colour);
	triangle->position(-size, -size, -size); // LL
	triangle->colour(colour);
	triangle->position(-size, size, -size); // UL
	triangle->colour(colour);


	triangle->index(0);
	triangle->index(2);
	triangle->index(1);

	triangle->index(2);
	triangle->index(0);
	triangle->index(3);

	triangle->index(4);
	triangle->index(5);
	triangle->index(6);

	triangle->index(6);
	triangle->index(7);
	triangle->index(4);

	triangle->index(0);
	triangle->index(1);
	triangle->index(4);

	triangle->index(1);
	triangle->index(5);
	triangle->index(4);

	triangle->index(3);
	triangle->index(6);
	triangle->index(2);

	triangle->index(3);
	triangle->index(7);
	triangle->index(6);

	triangle->index(4);
	triangle->index(7);
	triangle->index(3);

	triangle->index(3);
	triangle->index(0);
	triangle->index(4);

	triangle->index(2);
	triangle->index(6);
	triangle->index(1);

	triangle->index(1);
	triangle->index(6);
	triangle->index(5);

	// End drawing
	triangle->end();

	SceneNode *node = sceneManager->getRootSceneNode()->createChildSceneNode();
	node->createChild();
	node->attachObject(triangle);

	return new Planets(node);
}

void Planets::setRotate(double speed)
{
	rotationSpeed = Radian(Degree(speed));
}

void Planets::setRevolve(double speed)
{
	revolutionSpeed = Radian(Degree(speed));
}

void Planets::updatePosition(const FrameEvent & evt)
{
	float oldX = Node->getPosition().x - HeavenlyBodies->Node->getPosition().x;

	float oldZ = Node->getPosition().z - HeavenlyBodies->Node->getPosition().z;

	float newX = (oldX*Math::Cos((360 / 60 * revolutionSpeed)* evt.timeSinceLastFrame)) + (oldZ*Math::Sin((360 / 60 * revolutionSpeed)* evt.timeSinceLastFrame));

	float newZ = (oldX*-Math::Sin((360 / 60 * revolutionSpeed)*evt.timeSinceLastFrame)) + (oldZ*Math::Cos((360 / 60 * revolutionSpeed)* evt.timeSinceLastFrame));

	Degree Rotation = rotationSpeed;

	Node->yaw(Radian((360 / 60 * Rotation)* evt.timeSinceLastFrame));

	Node->setPosition(newX + HeavenlyBodies->Node->getPosition().x, Node->getPosition().y, newZ + HeavenlyBodies->Node->getPosition().z);
}

void Planets::setHead(Planets * HeavenlyBody)
{
	HeavenlyBodies = HeavenlyBody;
}

Planets & Planets::getHead()
{
	return *HeavenlyBodies;
}

SceneNode & Planets::getNode()
{
	return *Node;
}

